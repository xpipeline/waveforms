The following files are catalogs of supernova waveforms for the O3 
search: 

Abdikamalov2014.mat
Andresen2016.mat
Andresen2019.mat
Cerda-Duran2013.mat
Dimmelmeier2008.mat
Eggenberger2021.mat
Kuroda2016.mat
Kuroda2017.mat
Mezzacappa2020.mat
Morozova2018.mat
Mueller2012.mat
OConnor2018.mat
Ott2013.mat
Pan2020.mat
Powell2018.mat
Powell2020.mat
Radice2019.mat
Richers2017.mat
Scheidegger2010.mat
Yakunin2015.mat

The following scripts were used to make .mat file catalogs of these supernova 
waveforms. The original files are stored on CIT at /home/marek.szczepanczyk/git/sngw/
See https://wiki.ligo.org/Bursts/O3SearchWaveforms for more details.

  make_catalog_driver.m - driver script
  make_catalog.m - main script that processes one waveform set
  findclosest.m - helper function




