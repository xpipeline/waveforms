This git project is actually just a repository of waveform catalogs and related 
functions to be used by X-Pipeline:matlab/share/xmakewaveform.m.

Development notes / to-do:
--------------------------

CCSN waveforms: check these sources for the latest CCSN waveforms used by the
    burst group:
        https://wiki.ligo.org/Bursts/O3SearchWaveforms
        https://wiki.ligo.org/Bursts/SupernovaWorkingGroup#Waveforms
        https://wiki.ligo.org/Bursts/SNSearchWaveforms
    This repo may have usedful code:
        https://trac.ligo.caltech.edu/snsearch/browser#snsearch
    See also the file make_catalog_README.txt in this repository.
    The osnsearchdist.mat catalog shoul dbe kept for historical 
    comparisons but should be converted to the "universal" format.

CBC waveforms: inspiral* -> consolidate all these types into a single type, 
    'cbc', that uses IMRPHENOM to include merger and ringdown with no 
    discontinuities. Include optional 'approximant' argument (default imrphenom)
    to allow future versions to seelct from different approximants. This would 
    also allow us to drop support for lalinspiral and drop outputs a1, a2 (L1) 
    and optional input 'inclinationType' (L58)

ADI waveforms: The adi_compress.m script compresses adi-a/.../e to sparse sampling 
    interpolating over amplitude and phase. Potentially implement selection of 
    general parameters by calling adi_generate.py function, though this is slow 
    (run time ~3 min).

Other: look at developing code that can generate waveforms from stored moments.

Other waveforms: previously had catalogs based on these references:
- Long-duration signals from magnetars: https://wiki.ligo.org/Main/LongDurationRemnantWaveforms
- Eccentric binaries: https://dcc.ligo.org/DocDB/0140/G1700265/001/ecc_presentation.pdf 
- Postmerger signals: https://iopscience.iop.org/article/10.1088/0264-9381/33/8/085003/meta
The catalogs were deleted because they were untrustworthy and/or very old. Updated 
catalogs for these types of signals might be useful.
 

