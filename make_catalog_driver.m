
% ---- Specify target directory and waveform models.
baseDir = '/home/marek.szczepanczyk/git/sngw/';
%
simNameAll{1}     = 'Abdikamalov_2014';
modelNameAll{1}   = {'A1O01.0', 'A2O01.0', 'A3O01.0', 'A3O06.0', 'A4O01.0', 'A5O01.0'};
defaultDistanceAll{1} = 10; % [kpc]
%
simNameAll{2}     = 'Andresen_2016';
modelNameAll{2}   = {'s11', 's20', 's20s', 's27'};
defaultDistanceAll{2} = 10; % [kpc]
%
simNameAll{3}     = 'Andresen_2019';
modelNameAll{3}   = {'s15fr', 's15nr', 's15r'};
defaultDistanceAll{3} = 10; % [kpc]
%
simNameAll{4}     = 'Cerda-Duran_2013';
modelNameAll{4}   = {'fiducial', 'slow'};
defaultDistanceAll{4} = 10; % [kpc]
%
simNameAll{5}     = 'Dimmelmeier_2008';
modelNameAll{5}   = {'s15a2o05_ls', 's15a2o09_ls', 's15a3o15_ls', 's20a1o05_ls', ...
                    's20a3o09_ls', 's20a3o13_ls'};
defaultDistanceAll{5} = 10; % [kpc]
%
simNameAll{6}     = 'Eggenberger_Andersen_et_al2021_EOS_dependence_GWs';
modelNameAll{6}   = {'h_k200', 'h_k260', 'h_m0.55', 'h_m0.55r1', 'h_m0.55r2', ...
                    'h_m0.75', 'h_m0.95', 'h_m0.95r1', 'h_m0.95r2'};
defaultDistanceAll{6} = 10; % [kpc]
%
simNameAll{7}     = 'Kuroda_2016';
modelNameAll{7}   = {'SFHx', 'TM1'};
defaultDistanceAll{7} = 10; % [kpc]
%
simNameAll{8}     = 'Kuroda_2017';
modelNameAll{8}   = {'S11.2', 'S15.0'};  %-- kludge in make_catalog: extra files in waveform directory confuse script
defaultDistanceAll{8} = 10; % [kpc]
%
simNameAll{9}     = 'Morozova_2018';
modelNameAll{9}   = {'M10_LS220', 'M10_SFHo', 'M13_SFHo_multipole', 'M19_SFHo', 'M10_DD2', ...
                    'M10_LS220_no_manybody', 'M13_SFHo', 'M13_SFHo_rotating'};
defaultDistanceAll{9} = 10; % [kpc]
%
simNameAll{10}    = 'Mueller_2012';
modelNameAll{10}  = {'L15-3', 'N20-2', 'W15-4'};
defaultDistanceAll{10} = 10; % [kpc]
%
simNameAll{11}    = 'OConnor_2018';
modelNameAll{11}  = {'mesa20', 'mesa20_2D', 'mesa20_2D_pert', 'mesa20_LR', ...
                     'mesa20_pert', 'mesa20_pert_LR', 'mesa20_v_LR'};
defaultDistanceAll{11} = 10; % [kpc]
%
simNameAll{12}    = 'Ott_2013';
modelNameAll{12}  = {'s27_fheat1d00', 's27_fheat1d05', 's27_fheat1d10', 's27_fheat1d15'};
defaultDistanceAll{12} = 10; % [kpc]
%
simNameAll{13}    = 'Pan_2020';
modelNameAll{13}  = {'FR', 'NR', 'SR'};  %-- kludge in make_catalog: waveform file names do not follow convention of other sets
defaultDistanceAll{13} = 10; % [kpc]
%
simNameAll{14}    = 'Powell_2018';
modelNameAll{14}  = {'s18_3d', 's3.5_pns'};
defaultDistanceAll{14} = 10; % [kpc]
%
simNameAll{15}    = 'Powell_2020';
modelNameAll{15}  = {'m39', 's18', 'y20'}; %-- kludge in make_catalog: m39 waveform file names do not follow convention of other sets
defaultDistanceAll{15} = 10; % [kpc]
%
simNameAll{16}    = 'Radice_2019';
modelNameAll{16}  = {'s10', 's11', 's12', 's13', 's14', 's15', 's19', 's25', 's60', 's9'}; 
defaultDistanceAll{16} = 10; % [kpc]
%
simNameAll{17}    = 'Richers_2017';
modelNameAll{17}  = {'A300w0.50_BHBLP', 'A300w0.50_SFHx', 'A467w0.50_LS220', ... 
                     'A467w9.50_GShenFSU2.1', 'A467w9.50_SFHx', ...
                     'A300w0.50_GShenFSU2.1', 'A467w0.50_BHBLP', 'A467w0.50_SFHo', ...
                     'A467w9.50_HSDD2', 'A300w0.50_HSDD2', 'A467w0.50_GShenFSU2.1', ...
                     'A467w0.50_SFHx', 'A467w9.50_HSNL3', 'A300w0.50_LS220', ...
                     'A467w0.50_HSDD2', 'A467w5.50_HSNL3', 'A467w9.50_LS220', ...        
                     'A300w0.50_SFHo', 'A467w0.50_HSNL3', 'A467w9.50_BHBLP', ...
                     'A467w9.50_SFHo'};
defaultDistanceAll{17} = 10; % [kpc]
%
simNameAll{18}    = 'Scheidegger_2010';
modelNameAll{18}  = {'R1E1CA_L', 'R3E1AC_L', 'R4E1FC_L'};
defaultDistanceAll{18} = 10; % [kpc]
%
simNameAll{19}    = 'Yakunin_2015';
modelNameAll{19}  = {'B12', 'B15', 'B20', 'B25'};
defaultDistanceAll{19} = 10; % [kpc]
%
simNameAll{20}    = 'Mezzacappa_2020';
modelNameAll{20}  = {'C15-3D'};  %-- kludge in make_catalog: wrong directory structure
defaultDistanceAll{20} = 10; % [kpc]

% ---- Loop over simulation sets. Need to specify baseDir, simName, modelName
%      for make_catalog. 
for isim = 1:length(simNameAll)

    simName = simNameAll{isim};
    modelNames = modelNameAll{isim};
    defaultDistance = defaultDistanceAll{isim};
    
    for imod = 1:length(modelNames)
        modelName = modelNames{imod};
        make_catalog
        hplus{imod}  = hp;
        hcross{imod} = hc;
        sampleRate(imod) = fs;
    end

    % ---- Save catalog and metadata to a .mat file after renaming some variables
    %      for convenience.
    if isim==6
        catName = 'eggenberger2021';
    else
        catName = lower(strrep(simName,'_',''));
    end
    save([catName '.mat'],'baseDir','simName','catName','modelNames','hplus','hcross','sampleRate','defaultDistance');

    % ---- Clean up before starting next simName.
    clear h h2p ii phiStr targetDir h1 hc imod polStr theta catName h1c hcross ...
        jj result thetaStr fileName h1p  hp modelName simName tmpName fs h2 ...
        hplus status uPhi fsStr h2c idx phi uTheta sampleRate 
    
end

% ---- Perform sanity checks on new catalog files.
for isim = 1:length(simNameAll)
    
    simName = simNameAll{isim};
    if isim==6
        catName = 'eggenberger2021';
    else
        catName = lower(strrep(simName,'_',''));
    end
    chk = load([catName '.mat']);
    
    % ---- Checks on sample rate, angles, and waveform lengths.
    disp(['--- Catalog: ' catName ' --------------------------------'])
    if any(chk.sampleRate==0)
        disp(['Sample rate error: ' num2str(chk.sampleRate)])
    end
    for imod=1:length(chk.modelNames)
        if (chk.hplus{imod}.phi~=chk.hcross{imod}.phi) | (chk.hplus{imod}.theta~=chk.hcross{imod}.theta)
            disp(['Mismatch in sampled angles for model ' chk.modelNames{imod} '.'])
        end
        if size(chk.hplus{imod}.h,1)~=size(chk.hcross{imod}.h,1)
            disp(['Mismatch in length of plus and cross waveforms for model ' chk.modelNames{imod} '.'])
        end
    end   
    
    % ---- Check how theta=0,pi waveforms are handled for 2D simulations. They
    %      should be identically zero.
    for imod=1:length(chk.modelNames)
        disp([' 2D model: ' chk.modelNames{imod} ])
        if max(abs(chk.hcross{imod}.phi))==0
            % ---- Looks like 2D simulation.
            [max(abs(chk.hplus{imod}.h(:,1))), max(abs(chk.hplus{end}.h(:,end)))]
        end
    end
    
    % ---- Plot each waveform for a random point in emission pattern.
    for imod=1:length(chk.modelNames)
        ciota = 2*rand(1)-1;
        phi = 2*pi*rand(1);
        T = 2^(nextpow2(size(chk.hplus{imod}.h,1)/chk.sampleRate(imod))+1);
        catDir = '/Users/psutton/Documents/xpipeline/branches/waveforms';
        [t,hp,hc] = xmakewaveform(catName,[chk.modelNames{imod} '~10~' num2str(ciota) ...
            '~' num2str(phi)],T,T/2,16384,'catalogDirectory',catDir);
        figure; plot(t,[hp,hc]); grid on
        title([catName ' - ' chk.modelNames{imod}])
    end        

end
