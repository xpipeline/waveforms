% For waveform descriptions see https://wiki.ligo.org/Main/STAMPandCWB and
% Samuel Franco's thesis (LAL, 2014).

% ---- Note: the text files were obtained from
%      /home/drago/STAMP_COMPARISON/FRAMES/ at Caltech. 
%      These were nicely tapered at each end, but the EBBH waveforms have 
%      a length that is 2^n+1. Here we drop the last sample and reapply an
%      end taper to keep the waveform smooth. Each tapered EBBH waveform
%      was then checked visually for good smooth behaviour. 
%      The ADI waveforms are zero-padded to integer second duration.
taper = hann(32);
taper(1:16) = [];

% ---- Load each waveform text file.  
load STAMP_WF1_eBBH-A.dat
% tf = STAMP_WF1_eBBH_A(:,1);
hp = STAMP_WF1_eBBH_A(:,2);
hc = STAMP_WF1_eBBH_A(:,3);
% ---- Verify this is correct hrss at 1 Mpc.
% (sum(hp.^2+hc.^2)/16384)^0.5
% ---- Save every fourth sample (16384 Hz -> 4096 Hz); checked visually
%      that 4096 Hz rate looks high enough that we don't need fancier
%      resampling.
hp = hp(1:4:end-1);
hc = hc(1:4:end-1);
% ---- Taper.
hp(end-15:end) = hp(end-15:end).*taper;
hc(end-15:end) = hc(end-15:end).*taper;
fs = 4096;
comment = 'EBBH-A(1): M1=2, M2=2, e=0.99, distance=1Mpc';
save('ebbh-a.mat','hp','hc','fs','comment');

% ---- Load each waveform text file.  
load STAMP_WF2_eBBH-D.dat
% tf = STAMP_WF2_eBBH_D(:,1);
hp = STAMP_WF2_eBBH_D(:,2);
hc = STAMP_WF2_eBBH_D(:,3);
% ---- Verify this is correct hrss at 1 Mpc.
% (sum(hp.^2+hc.^2)/16384)^0.5
% ---- Save every fourth sample (16384 Hz -> 4096 Hz); checked visually
%      that 4096 Hz rate looks high enough that we don't need fancier
%      resampling.
hp = hp(1:4:end-1);
hc = hc(1:4:end-1);
% ---- Taper.
hp(end-15:end) = hp(end-15:end).*taper;
hc(end-15:end) = hc(end-15:end).*taper;
fs = 4096;
comment = 'EBBH-D(2): M1=6, M2=6, e=0.94, distance=1Mpc';
save('ebbh-d.mat','hp','hc','fs','comment');

% ---- Load each waveform text file.  
load STAMP_WF3_eBBH-E.dat
% tf = STAMP_WF3_eBBH_E(:,1);
hp = STAMP_WF3_eBBH_E(:,2);
hc = STAMP_WF3_eBBH_E(:,3);
% ---- Verify this is correct hrss at 1 Mpc.
% (sum(hp.^2+hc.^2)/16384)^0.5
% ---- Save every fourth sample (16384 Hz -> 4096 Hz); checked visually
%      that 4096 Hz rate looks high enough that we don't need fancier
%      resampling.
hp = hp(1:4:end-1);
hc = hc(1:4:end-1);
% ---- Taper.
hp(end-15:end) = hp(end-15:end).*taper;
hc(end-15:end) = hc(end-15:end).*taper;
fs = 4096;
comment = 'EBBH-E(3): M1=18, M2=10, e=0.85, distance=1Mpc';
save('ebbh-e.mat','hp','hc','fs','comment');

% ---- Load each waveform text file.  
load STAMP_WF4_ADI-B.dat
% tf = STAMP_WF4_ADI_B(:,1);
hp = STAMP_WF4_ADI_B(:,2);
hc = STAMP_WF4_ADI_B(:,3);
% ---- Verify this is correct hrss at 1 Mpc.
% (sum(hp.^2+hc.^2)/16384)^0.5
% ---- Save every fourth sample (16384 Hz -> 4096 Hz); checked visually
%      that 4096 Hz rate looks high enough that we don't need fancier
%      resampling.
hp = hp(1:4:end);
hc = hc(1:4:end);
fs = 4096;
% ---- Zero-pad to integer duration.
N = ceil(length(hp)/fs)*fs;
hp(N) = 0;
hc(N) = 0;
comment = 'ADI-B(4): M_BH=10, a=0.95, eps=0.2, M_disk=1.5, distance=1Mpc';
save('adi-b.mat','hp','hc','fs','comment');

% ---- Load each waveform text file.  
load STAMP_WF5_ADI-A.dat
% tf = STAMP_WF5_ADI_A(:,1);
hp = STAMP_WF5_ADI_A(:,2);
hc = STAMP_WF5_ADI_A(:,3);
% ---- Verify this is correct hrss at 1 Mpc.
% (sum(hp.^2+hc.^2)/16384)^0.5
% ---- Save every fourth sample (16384 Hz -> 4096 Hz); checked visually
%      that 4096 Hz rate looks high enough that we don't need fancier
%      resampling.
hp = hp(1:4:end);
hc = hc(1:4:end);
fs = 4096;
% ---- Zero-pad to integer duration.
N = ceil(length(hp)/fs)*fs;
hp(N) = 0;
hc(N) = 0;
comment = 'ADI-A(5): M_BH=5, a=0.3, eps=0.05, M_disk=1.5, distance=1Mpc';
save('adi-a.mat','hp','hc','fs','comment');

% ---- Load each waveform text file.  
load STAMP_WF6_ADI-E.dat
% tf = STAMP_WF6_ADI_E(:,1);
hp = STAMP_WF6_ADI_E(:,2);
hc = STAMP_WF6_ADI_E(:,3);
% ---- Verify this is correct hrss at 1 Mpc.
% (sum(hp.^2+hc.^2)/16384)^0.5
% ---- Save every fourth sample (16384 Hz -> 4096 Hz); checked visually
%      that 4096 Hz rate looks high enough that we don't need fancier
%      resampling.
hp = hp(1:4:end);
hc = hc(1:4:end);
fs = 4096;
% ---- Zero-pad to integer duration.
N = ceil(length(hp)/fs)*fs;
hp(N) = 0;
hc(N) = 0;
comment = 'ADI-E(6): M_BH=8, a=0.99, eps=0.065, M_disk=1.5, distance=1Mpc';
save('adi-e.mat','hp','hc','fs','comment');

% ---- Load each waveform text file.  
load STAMP_WF11_ADI-D.dat
% tf = STAMP_WF11_ADI_D(:,1);
hp = STAMP_WF11_ADI_D(:,2);
hc = STAMP_WF11_ADI_D(:,3);
% ---- Verify this is correct hrss at 1 Mpc.
% (sum(hp.^2+hc.^2)/16384)^0.5
% ---- Save every fourth sample (16384 Hz -> 4096 Hz); checked visually
%      that 4096 Hz rate looks high enough that we don't need fancier
%      resampling.
hp = hp(1:4:end);
hc = hc(1:4:end);
fs = 4096;
% ---- Zero-pad to integer duration.
N = ceil(length(hp)/fs)*fs;
hp(N) = 0;
hc(N) = 0;
comment = 'ADI-D(11): M_BH=3, a=0.7, eps=0.035, M_disk=1.5, distance=1Mpc';
save('adi-d.mat','hp','hc','fs','comment');

% ---- Load each waveform text file.  
load STAMP_WF12_ADI-C.dat
% tf = STAMP_WF12_ADI_C(:,1);
hp = STAMP_WF12_ADI_C(:,2);
hc = STAMP_WF12_ADI_C(:,3);
% ---- Verify this is correct hrss at 1 Mpc.
% (sum(hp.^2+hc.^2)/16384)^0.5
% ---- Save every fourth sample (16384 Hz -> 4096 Hz); checked visually
%      that 4096 Hz rate looks high enough that we don't need fancier
%      resampling.
hp = hp(1:4:end);
hc = hc(1:4:end);
fs = 4096;
% ---- Zero-pad to integer duration.
N = ceil(length(hp)/fs)*fs;
hp(N) = 0;
hc(N) = 0;
comment = 'ADI-C(12): M_BH=10, a=0.95, eps=0.04, M_disk=1.5, distance=1Mpc';
save('adi-c.mat','hp','hc','fs','comment');

