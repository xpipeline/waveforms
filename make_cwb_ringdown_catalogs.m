% ---- Script to make .mat catalog files that contain ringdown waveforms following the
%      definition used by cWB here:
%        https://git.ligo.org/yumeng.xu/burst_waveform/-/blob/main/burst_waveform/models/ringdown.py?ref_type=heads
%      See also
%        https://git.ligo.org/yumeng.xu/burst_waveform/-/blob/main/notebook/Ringdown.ipynb?ref_type=heads
%      This is intended for use in the O4b Vela glitch search.

% ---- Injection sets from the .ini file.
% % ---- ringdowns: short-duration, fixed inclination = 63.3 deg or 116.7 deg.
% ringdown2is = ds2p!1.0e-21~0.01~650~0;6.2832;linear~0.4493 
% ringdown4is = ds2p!1.0e-21~0.01~2100~0;6.2832;linear~-0.4493 
% % ---- ringdowns: medium-duration, fixed inclination = 63.3 deg or 116.7 deg.
% ringdown1i = ds2p!1.0e-21~0.1~290~0;6.2832;linear~0.4493
% ringdown2i = ds2p!1.0e-21~0.1~650~0;6.2832;linear~-0.4493
% ringdown3i = ds2p!1.0e-21~0.1~1100~0;6.2832;linear~0.4493
% ringdown4i = ds2p!1.0e-21~0.1~2100~0;6.2832;linear~-0.4493
% ringdown5i = ds2p!1.0e-21~0.1~3900~0;6.2832;linear~0.4493
% % ---- ringdowns: long-duration, fixed inclination = 63.3 deg or 116.7 deg.
% ringdown2il = ds2p!1.0e-21~0.5~650~0;6.2832;linear~-0.4493
% ringdown4il = ds2p!1.0e-21~0.5~2100~0;6.2832;linear~0.4493

% ---- Sets of ringdowns.
waveformLetter = {'a','b','c','d','e','f','g','h','i'};
waveform = {'ringdown2is','ringdown4is','ringdown1i','ringdown2i','ringdown3i', ...
            'ringdown4i','ringdown5i','ringdown2il','ringdown4il'};
tau = [0.01, 0.01, 0.1, 0.1, 0.1,  0.1,  0.1,  0.5, 0.5];
f0  = [650,  2100, 290, 650, 1100, 2100, 3900, 650, 2100];

% ---- Timeseries parameters common to all injections. The longest tau is 0.5 sec. Making
%      the duration 8 sec at starting the ringdown at 1 sec gives us a dampening factor of
%      ~exp(-14)~1e-6 by the end of the timeseries. 
% ---- Duration of timeseries [s].
T      = 8;
% ---- Start of ringdown [s].
T0     = 1;
% ---- Sample rate [Hz].
fs     = 16384;
% ---- Peak amplitude of the envelope [dimensionless].
h_peak = 1;
% ---- Cosine of inclination angle [dimensionless]. Generate at optimal orientation;
%      inclination factors will be applied in xmakewaveform.
ciota  = 1;

% ---- Derived parameters.
% ---- Number of data samples.
N = T * fs;
% ---- Vector of sample times, starting from zero.
t = (0:(N-1))'/fs;

% ----------------------------------------------------------------------------------------

for iwave = 1:length(waveform)

    comment = [waveform{iwave} ' (tau=' num2str(tau(iwave)) 's, f0=' num2str(f0(iwave)) 'Hz)'];

    % ---- Generate waveform for all times, inclduing unphysical (t<T0) times.
    hp = h_peak * 0.5*(1+ciota^2) * cos(2*pi*(t-T0)*f0(iwave)) .* exp(-(t-T0)/tau(iwave));
    hc = h_peak *     ciota       * sin(2*pi*(t-T0)*f0(iwave)) .* exp(-(t-T0)/tau(iwave));
    
    % ---- Smooth turn on: zero out t<T0 portion of sine wave and t<T0+1/(4*f0) portion of
    %      cosine wave. 
    idx = t<(T0+1/(4*f0(iwave)));
    hp(idx) = 0;
    idx = t<T0;
    hc(idx) = 0;
    
    figure; 
    pos = get(gcf,'position');
    set(gcf,'position',[pos(1:2) 920 420]);
    subplot(1,2,1); plot(t,[hp,hc],'linewidth',2)
    grid on
    axis([T0-1/(f0(iwave)) T0+4/(f0(iwave)) -1.1*h_peak 1.1*h_peak])
    set(gca,'fontsize',16)
    xlabel('time [sec]')
    ylabel('strain')
    title(comment)
    %
    subplot(1,2,2); plot(t,[hp,hc],'linewidth',2)
    grid on
    axis([T0-tau(iwave) T0+5*tau(iwave) -1.1*h_peak 1.1*h_peak])
    set(gca,'fontsize',16)
    xlabel('time [sec]')
    ylabel('strain')
    title(comment)

    % ---- Save catalog.
    save(['usercreated-' waveformLetter{iwave} '.mat'],'hp','hc','fs','comment');

    % ---- Report to screen.
    % ---- Analytical hrss (ignoring partial zeroing of cosine wave).
    hrss_analytic = h_peak / (2/tau(iwave))^0.5;
    % ---- Empirical hrss.
    hrss_empiric = (sum((hp.^2+hc.^2)/fs))^0.5;
    disp(comment)
    disp(['Analytical hrss: ' num2str(hrss_analytic)])
    disp(['Empirical hrss:  ' num2str(hrss_empiric)])

end
