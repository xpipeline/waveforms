
% ---- Specify target directory and waveform model name.
% baseDir = './';
% simName = 'Andresen_2016';
% modelName = 's11';

% ------------------------------------------------------------------------------
%    Read target directory and load data.
% ------------------------------------------------------------------------------

% ---- Target directory.
targetDir = [baseDir simName '/' modelName '/' modelName '/'];
if strcmp(simName,'Mezzacappa_2020')
    % ---- Kludge to handle non-standard directory structure.
    targetDir = [baseDir simName '/' modelName '/' modelName '/' modelName '/'];
end

% ---- Get list of files in target directory.
[status,result] = system(['ls ' targetDir]);
fileName = cell(0);
while ~isempty(result)
    [tmpName,result] = strtok(result);
    if ~isempty(tmpName) && ~(strcmp(simName,'Kuroda_2017') & strcmp(tmpName(1:11),'Kuroda_2017'))
        fileName{end+1} = [targetDir tmpName];
    end
end

% ---- Initialise storage.
hp.phi = [];
hp.theta = [];
hp.h = [];
hc.phi = [];
hc.theta = [];
hc.h = [];
fs = [];

% ---- Loop over files, loading each and storing meta-data.
for ii=1:length(fileName)
    tmpName = fileName{ii};
    % ---- Extract sky position, sampling rate, and polarisation from file name.
    if strcmp(simName,'Pan_2020')
        % ---- Special case for non-standard file name.
        subName = tmpName(length(targetDir)+length(simName)+1+length(modelName)+2:end-4);
    elseif strcmp(simName,'Powell_2020') & strcmp(modelName,'m39')
        % ---- Special case for non-standard file name.
        subName = tmpName(length(targetDir)+length(modelName)+5:end-4);
    else
        subName = tmpName(length(targetDir)+length(modelName)+2:end-4);
    end        
    [phiStr,subName] = strtok(subName,'_');
    phi = str2num(phiStr(4:end));
    [thetaStr,subName] = strtok(subName,'_');
    theta = str2num(thetaStr(6:end));
    [fsStr,subName] = strtok(subName,'_');
    fs(end+1) = str2num(fsStr(1:end-2));
    polStr = strtok(subName,'_');
    % ---- Load h(t) from file.
    h = load(fileName{ii});
    % ---- Store data in the appropriate arrays.
    switch(polStr)
        case 'hplus'
            hp.phi(end+1)   = phi;
            hp.theta(end+1) = theta;
            hp.h(:,end+1)   = h;  %-- this will fail if files have variable length
        case 'hcross'
            hc.phi(end+1)   = phi;
            hc.theta(end+1) = theta;
            hc.h(:,end+1)   = h;  %-- this will fail if files have variable length
        otherwise
            error(['Polarisation string ' polStr ' not recognised.']);
    end
end

% ---- Sort data by sampled sky position (firstby theta then by phi). The 'ls'
%      command can return file names in a random order. Re-ordering makes checks
%      easier. 
[~,I] = sortrows([hp.theta(:) hp.phi(:)]);
hp.phi   = hp.phi(I);
hp.theta = hp.theta(I);
hp.h     = hp.h(:,I);
[~,I] = sortrows([hc.theta(:) hc.phi(:)]);
hc.phi   = hc.phi(I);
hc.theta = hc.theta(I);
hc.h     = hc.h(:,I);


% ------------------------------------------------------------------------------
%    Checks.
% ------------------------------------------------------------------------------

disp('***************************************************************************')
disp(['Checking ' simName ' - ' modelName ' catalog.'])

% ---- Verify that sampling rate is the same for all waveforms.
if length(unique(fs))~=1
    error(['Multiple sample rates found: num2str(fs) '.']);
end
fs = fs(1);

% ---- Perform sanity checks on the waveforms: 
%       -> At theta = 0 all phi  values should give the same waveform (after
%          correcting for change in polarisation angle). 
%       -> At theta = pi all phi  values should give the same waveform (after
%          correcting for change in polarisation angle). 
%       -> For any theta the phi=0 and phi=2*pi waveforms should be identical.

% ---- Useful shorthand: unique grid values of phi, theta.
uPhi = unique(hp.phi);
uTheta = unique(hp.theta);
    
% ---- Check equivalence of all phi for theta=0.
disp('Checking equivalence of all phi for theta=0:')
phi = uPhi(1);
theta = 0;
idx = findclosest(hp.phi,hp.theta,phi,theta);
h1p = hp.h(:,idx);    
h1c = hc.h(:,idx);
maxErr = 0;
for jj=2:length(uPhi)
    phi = uPhi(jj);
    idx = findclosest(hp.phi,hp.theta,phi,theta);
    h2p = hp.h(:,idx);
    h2c = hc.h(:,idx);
    % ---- To compare, remember to correct for polarisation change:
    %        Fp' =  cos(2*psi) Fp + sin(2*psi) Fc
    %        Fc' = -sin(2*psi) Fp + cos(2*psi) Fc
    %      Note sign difference c.f. other check (below) which is at south pole.
    maxErr = max(maxErr,norm(( cos(2*(phi-uPhi(1)))*h2p-sin(2*(phi-uPhi(1)))*h2c)-h1p)./norm(h1p));
    maxErr = max(maxErr,norm(( sin(2*(phi-uPhi(1)))*h2p+cos(2*(phi-uPhi(1)))*h2c)-h1c)./norm(h1c));
end
disp(['    Maximum fractional error in norm: ' num2str(maxErr)'.']);

% ---- Check equivalence of all phi for theta=pi.
disp('Checking equivalence of all phi for theta=pi:')
phi = uPhi(1);
theta = 3.14;
idx = findclosest(hp.phi,hp.theta,phi,theta);
h1p = hp.h(:,idx);    
h1c = hc.h(:,idx); 
maxErr = 0;
for jj=2:length(uPhi)
    phi = uPhi(jj);
    idx = findclosest(hp.phi,hp.theta,phi,theta);
    h2p = hp.h(:,idx);
    h2c = hc.h(:,idx);
    % ---- To compare, remember to correct for polarisation change:
    %        Fp' =  cos(2*psi) Fp + sin(2*psi) Fc
    %        Fc' = -sin(2*psi) Fp + cos(2*psi) Fc
    %      Note sign difference c.f. other check (above) which is at north pole.
    maxErr = max(maxErr,norm(( cos(2*(phi-uPhi(1)))*h2p+sin(2*(phi-uPhi(1)))*h2c)-h1p)./norm(h1p));
    maxErr = max(maxErr,norm((-sin(2*(phi-uPhi(1)))*h2p+cos(2*(phi-uPhi(1)))*h2c)-h1c)./norm(h1c));
end
disp(['    Maximum fractional error in norm: ' num2str(maxErr)'.']);

% ---- Check equivalence of phi=0 and phi=6.283 waveforms for each theta.
disp('Checking equivalence of phi=0 & phi=6.283 waveforms for each theta:')
maxErr = 0;
for jj=1:length(uTheta)
    phi = uPhi(1);
    idx = findclosest(hp.phi,hp.theta,phi,theta);
    h1 = hp.h(:,idx);
    phi = uPhi(end);
    idx = findclosest(hp.phi,hp.theta,phi,theta);
    h2 = hp.h(:,idx);
    maxErr = max(maxErr,norm(h1-h2)./norm(h1));
end
disp(['    Maximum fractional error in norm: ' num2str(maxErr)'.']);

% disp('***************************************************************************')


% ------------------------------------------------------------------------------

