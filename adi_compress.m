% ---- This script studies options for semi-analytic fits to the ADI-a...e
%      waveforms. Results:
%      1. Polynomial fits to amplitude and frequency or phase don't work: the 
%         low-order polynomials just aren't able to get the phase accurate to
%         ~0.01Hz required to keep phase coherence to ~1 cycle over ~100sec.
%      2. Cubic spline interpolation of amplitude and phase with sampling rate 
%         of 16 / 32 / 128 Hz gives >0.995 / >0.998 / >0.9998 matches. The 
%         mismatch happens at the ends where the interpolated waveform amplitude 
%         ramps up more slowly over a few cycles and is by far the largest for 
%         adi-b because that is the shortest waveform.
%
% $Id$

close all; clear all

catDir = '../../branches/waveforms/';

% ---- Select one or more waveforms to model.
model = {'a','b','c','d','e'};
% model = {'b'};

% ---- Select one interpolation option.
%interp1method = 'makima';
interp1method = 'spline';
%interp1method = 'linear';

% ---- How many points do we record?
% downsampled_fs = 128; %-- approx 1 sample per cycle.
% downsampled_fs = 32; %-- approx 1 sample per 4 cycles.
downsampled_fs = 16; %-- approx 1 sample per 30 cycles.


% --------------------------------------------------------------------------
%       Loop over waveforms.
% --------------------------------------------------------------------------

for imodel = 1:length(model)
    
    % --------------------------------------------------------------------------
    %      Load the desired waveform and required metadata (end time).
    % --------------------------------------------------------------------------

    switch model{imodel}
        case 'a'
            load([catDir 'adi-a.mat'])
            t_end = 38.7797241211;
        case 'b'
            load([catDir 'adi-b.mat'])
            t_end = 9.43463134766;
        case 'c'
            load([catDir 'adi-c.mat'])
            t_end = 235.864746094;
        case 'd'
            load([catDir 'adi-d.mat'])
            t_end = 142.042907715;
        case 'e'
            load([catDir 'adi-e.mat'])
            t_end = 76.2574462891;
    end
    N_down = ceil(t_end * downsampled_fs);

    % --------------------------------------------------------------------------
    %      Extract summary data from catalog waveform: amplitude, frequency, and
    %      phase versus time sample, once per waveform period.
    % --------------------------------------------------------------------------

    % ---- Find index and time of all - -> + transitions in the plus polarisation.
    idx_n2p = find(hp(1:end-1)<=0 & hp(2:end)>0);
    % ---- Interpolate to find ~exact position of zero crossing.
    N = length(idx_n2p);
    zero_idx = zeros(N,1);
    for ii=1:N
        jj = idx_n2p(ii);
        % ---- Caveat: we should check that ii is not 1, end-1, or end and that 
        %      the range jj-1:jj+2 does not include idx_n2p(ii+/-1).
        idx = jj-1:jj+2;
        % ---- Effective index of zero-crossing (non-integer).
        zero_idx(ii) = interp1(hp(idx),idx,0,interp1method);
        % z(ii) = interp1(hp(idx_n2p(ii):(idx_n2p(ii)+1)),[idx_n2p(ii) idx_n2p(ii)+1]',0,interp1method);
    end
    % ---- Times of zero crossing, in seconds. The -1 is beacause index=1 corresponds to time=0.
    zero_time = (zero_idx-1)/fs;

    % ---- Instantaneous phase at each zero-crossing, starting from 0.
    phase = 2*pi*[0:N-1]';

    % ---- We need to extend the range of sampled phase values to t=0 and t+t_end
    %      or else the interpolations below will given NaN values near there cutting off
    %      the waveform sharply. Do this by estimating the frequency at the ends.
    % ---- Instantaneous period and frequency from this zero-crossing to the next.
    %      Period: mean of time_from_previous_zero and time_to_next_zero (symmetric).
    %        period[i] = [(z[i]-z[i-1])+(z[i+1]-z[i])]/2 = [z[i+1]-z[i-1]]/2
    %      For teh first and last entries use to one-sided estimate.
    %        period[1] = z[2]-z[1]
    %        period[end] = z[end]-z[end-1]
    idx = 2:N-1;
    period = zeros(N,1);
    period(1)   = zero_time(2) - zero_time(1);
    period(idx) = (zero_time(idx+1) - zero_time(idx-1))/2; %-- we don't need this
    period(N)   = zero_time(N) - zero_time(N-1);
    frequency = 1./period;
    % ---- Extend sampled phase vector.
    phase = [phase(1)-2*pi*frequency(end)*zero_time(1); ...
             phase; ...
             phase(end)+2*pi*frequency(end)*(t_end-zero_time(end))];
    % ---- Update sampled time vector.
    zero_time = [0; zero_time; t_end];
    
    % ---- Instantaneous amplitude at each of the original waveform samples. (We 
    %      don't use the hp-only amplitude at zero_time because those are all 0 by
    %      construction!) 
    amplitude = (hp.^2+hc.^2).^0.5;
    % ---- Define times.
    amplitude_time = [0:(length(hp)-1)]'/fs;
    
    % --------------------------------------------------------------------------
    %      Downsample interpolated amplitude and frequency (new catalog data).
    % --------------------------------------------------------------------------

    % ---- How many points do we record?
    t_down = [0:1/N_down:1]'*t_end;
    % ---- Amplitude. Reset first and last values to neighboring values because
    %      catalog waveform is tapered to zero at the ends.
    amplitude_down = interp1(amplitude_time,amplitude,t_down,interp1method);
    % ---- Phase. Reset first and last phase values to sensible values.
    phase_down = interp1(zero_time,phase,t_down,interp1method);
    
    % --------------------------------------------------------------------------
    %      Reconstruct waveform from fit data at the original time samples.
    % --------------------------------------------------------------------------

    % ---- Make a new waveform using the interpolated samples. We reconstruct 
    %      using linear interpolation to avoid fatal over shoots at the end.
    rec_amplitude = interp1(t_down,amplitude_down,amplitude_time,'linear'); 
    rec_phase     = interp1(t_down,phase_down,    amplitude_time,'linear');
    gp = rec_amplitude.*sin(rec_phase);
    gc = rec_amplitude.*sin(rec_phase+pi/2);
    bad = find(isnan(gp) | isnan(gc)); %-- this tends to happen at the end due to extrapolation
    gp(bad) = 0;
    gc(bad) = 0;

    % ---- Check match with original waveforms.
    match_pp = hp'*gp/norm(hp)/norm(gp);
    match_cc = hc'*gc/norm(hc)/norm(gc);
    
    % --------------------------------------------------------------------------
    %      Make plots.
    % --------------------------------------------------------------------------

    H = [hp,hc];
    G = [gp,gc];
    M = [match_pp,match_cc];
    name = {'plus','cross'};
    figure; set(gcf,'Color',[1 1 1]);
    set(gcf,'position',get(gcf,'position').*[1 1 2 1.5])
    for ipol=1:2

        subplot(2,4,1+(ipol-1)*4)
        plot(amplitude_time,H(:,ipol));
        grid on; hold on
        plot(amplitude_time,G(:,ipol),'g-');
        ylabel('strain')
        axis([0 2/downsampled_fs get(gca,'ylim')])
        set(gca,'fontsize',16); 

        subplot(2,4,[2:3]+(ipol-1)*4)
        plot(amplitude_time,H(:,ipol));
        grid on; hold on
        plot(amplitude_time,G(:,ipol),'g-');
        legend('original',['fit (match=' num2str(M(:,ipol)) ')'])
        title(['ADI-' model{imodel} ' ' name{ipol} ' polarisation']);
        xlabel('time (s)'); set(gca, 'YTick', [])
        set(gca,'fontsize',16); 

        subplot(2,4,4+(ipol-1)*4)
        plot(amplitude_time,H(:,ipol));
        grid on; hold on
        plot(amplitude_time,G(:,ipol),'g-');
        set(gca,'fontsize',16); 
        set(gca, 'YTick', [])
        axis([t_end-2/downsampled_fs t_end+2/downsampled_fs get(gca,'ylim')])
    end

    saveas(gcf,['adi-' model{imodel} '_approx.png'],'png');

end
